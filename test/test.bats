#!/usr/bin/env bats
#
# Test SCP
#

set -e

# Can be run multiple times and takes an arg to add to the file contents to differentiate runs
fileSetup() {
  # parameters
  IMAGE_NAME=${DOCKERHUB_IMAGE}:${DOCKERHUB_TAG}

  # local variables
  RANDOM_NUMBER=$RANDOM
  [[ "${RANDOM_NUMBER}" -ne "" ]]
  BASE_LOCAL_DIR="tmp"
  LOCAL_DIR="$BASE_LOCAL_DIR/scp/"
  FILE_CONTENTS="Pipelines is awesome ${RANDOM_NUMBER}! $1"

  # clean up & create paths
  rm -rf $BASE_LOCAL_DIR

  mkdir -p $LOCAL_DIR/subdir
  FILE1=deployment-${RANDOM_NUMBER}-1.txt
  FILE2=deployment-${RANDOM_NUMBER}-2.txt
  FILE3=subdir/deployment-${RANDOM_NUMBER}-3.txt
  echo $FILE_CONTENTS > $LOCAL_DIR/$FILE1
  echo $FILE_CONTENTS > $LOCAL_DIR/$FILE2
  echo $FILE_CONTENTS > $LOCAL_DIR/$FILE3

  TARGET_HOST=52.65.134.106
  SSH_CONFIG_DIR=/opt/atlassian/pipelines/agent/ssh
}

# Simulate a known hosts failure by removing known_hosts entries.
badKnownHostsSetup() {
  BAD_SSH_CONFIG_DIR=/opt/atlassian/pipelines/agent/build/bad_ssh
  mkdir ${BAD_SSH_CONFIG_DIR}
  cp -rL ${SSH_CONFIG_DIR}/* ${BAD_SSH_CONFIG_DIR}
  echo "" > ${BAD_SSH_CONFIG_DIR}/known_hosts
}

teardown() {
  # clean up
  rm -rf $BASE_LOCAL_DIR
}

@test "Happy Path" {
  fileSetup run1

  # execute tests
  run docker run \
      -e USER="ec2-user" \
      -e SERVER="${TARGET_HOST}" \
      -e REMOTE_PATH="/var/www/html" \
      -e LOCAL_PATH="$LOCAL_DIR" \
      -v ${SSH_CONFIG_DIR}:${SSH_CONFIG_DIR} \
      -v $(pwd):$(pwd) \
      -w $(pwd) \
      $IMAGE_NAME
  [[ "${status}" == "0" ]]

  # verify
  run curl -s http://${TARGET_HOST}/scp/$FILE1
  [[ "${status}" == "0" ]]
  [[ "${output}" == "${FILE_CONTENTS}" ]]

  run curl -s http://${TARGET_HOST}/scp/$FILE2
  [[ "${status}" == "0" ]]
  [[ "${output}" == "${FILE_CONTENTS}" ]]

  run curl -s http://${TARGET_HOST}/scp/$FILE3
  [[ "${status}" == "0" ]]
  [[ "${output}" == "${FILE_CONTENTS}" ]]
}

@test "Bad user should fail" {
  fileSetup run2

  # execute tests
  run docker run \
      -e USER="ec2-user-doesnt-exist" \
      -e SERVER="${TARGET_HOST}" \
      -e REMOTE_PATH="/var/www/html" \
      -e LOCAL_PATH="$LOCAL_DIR" \
      -v ${SSH_CONFIG_DIR}:${SSH_CONFIG_DIR} \
      -v $(pwd):$(pwd) \
      -w $(pwd) \
      $IMAGE_NAME
  [[ "${status}" != "0" ]]
}

@test "Bad known_hosts should fail" {
  fileSetup run3
  badKnownHostsSetup

  # execute tests
  run docker run \
      -e USER="ec2-user-doesnt-exist" \
      -e SERVER="${TARGET_HOST}" \
      -e REMOTE_PATH="/var/www/html" \
      -e LOCAL_PATH="$LOCAL_DIR" \
      -e DEBUG="true" \
      -v ${BAD_SSH_CONFIG_DIR}:${SSH_CONFIG_DIR} \
      -v $(pwd):$(pwd) \
      -w $(pwd) \
      $IMAGE_NAME
  [[ "${status}" != "0" ]]
}


@test "Pass ssh key" {
  fileSetup run5

  # execute tests
  run docker run \
      -e USER="ec2-user" \
      -e SERVER="${TARGET_HOST}" \
      -e REMOTE_PATH="/var/www/html" \
      -e LOCAL_PATH="$LOCAL_DIR" \
      -e SSH_KEY="${SSH_KEY}" \
      -e DEBUG="true" \
      -v ${SSH_CONFIG_DIR}:${SSH_CONFIG_DIR} \
      -v $(pwd):$(pwd) \
      -w $(pwd) \
      $IMAGE_NAME
  [[ "${status}" == "0" ]]

  # verify
  run curl -s http://${TARGET_HOST}/scp/$FILE1
  [[ "${status}" == "0" ]]
  [[ "${output}" == "${FILE_CONTENTS}" ]]

  run curl -s http://${TARGET_HOST}/scp/$FILE2
  [[ "${status}" == "0" ]]
  [[ "${output}" == "${FILE_CONTENTS}" ]]

  run curl -s http://${TARGET_HOST}/scp/$FILE3
  [[ "${status}" == "0" ]]
  [[ "${output}" == "${FILE_CONTENTS}" ]]
}
